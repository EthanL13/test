package com.example.yygh.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * @author Ethan
 * @date 2022/1/3 11:49
 */
@Configuration
@EnableSwagger2
public class Swagger2Config {

    @Bean
    public Docket webApiConfig() {
        return new Docket(DocumentationType.SWAGGER_2)
            .groupName("webApi")
            .apiInfo(webApiInfo())
            .select()
            .paths(PathSelectors.any())
//            .paths(PathSelectors.regex("/api/.*"))
            .build();
    }

    private ApiInfo webApiInfo() {
        return new ApiInfoBuilder()
            .title("网站-Api文档")
            .description("本文描述了网站微服务接口定义")
            .version("1.0")
            .contact(new Contact("example", "http://example.com", "710234523@qq.com"))
            .build();
    }

    @Bean
    public Docket adminConfig() {
        return new Docket(DocumentationType.SWAGGER_2)
            .groupName("adminApi")
            .apiInfo(adminApiInfo())
            .select()
            .paths(PathSelectors.regex("/admin/.*"))
            .build();

    }

    private ApiInfo adminApiInfo() {
        return new ApiInfoBuilder()
            .title("后台管理系统-Api文档")
            .description("本文描述了后台管理系统微服务接口定义")
            .version("1.0")
            .contact(new Contact("example", "http://example.com", "710234523@qq.com"))
            .build();
    }

}
