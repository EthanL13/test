package com.example.yygh.hosp.controller;

import com.example.yygh.hosp.service.HospitalSetService;
import com.example.yygh.model.hosp.HospitalSet;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Ethan
 * @date 2022/1/3 11:33
 */
@Api("医院设置管理")
@RestController
@RequestMapping("/admin/hosp/hospitalSet")
public class HospitalSetController {
    @Autowired
    private HospitalSetService hospitalSetService;

    /**
     *1. 查询医院设置表里的信息
     *@author Ethan
     * @date 2022/1/3 11:38
     * @return null
     */
    @ApiOperation("获取所有设置")
    @GetMapping("findAll")
    public List<HospitalSet> findAll(){
        return hospitalSetService.list();
    }

    @DeleteMapping("{id}")
    @ApiOperation(value = "逻辑删除设置")
    public boolean removeHospSet(@PathVariable("id") Long id) {
        return hospitalSetService.removeById(id);
    }
}

