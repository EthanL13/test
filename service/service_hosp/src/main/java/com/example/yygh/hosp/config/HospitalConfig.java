package com.example.yygh.hosp.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Ethan
 * @date 2022/1/3 11:40
 */
@Configuration
@MapperScan("com.example.yygh.hosp.mapper")
public class HospitalConfig {
}
