package com.example.yygh.hosp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.yygh.model.hosp.HospitalSet;

/**
 * @author Ethan
 * @date 2022/1/3 11:23
 */
public interface HospitalSetMapper extends BaseMapper<HospitalSet> {
}
