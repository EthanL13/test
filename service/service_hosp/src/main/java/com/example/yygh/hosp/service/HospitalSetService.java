package com.example.yygh.hosp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.yygh.model.hosp.HospitalSet;

/**
 * @author Ethan
 * @date 2022/1/3 11:28
 */
public interface HospitalSetService extends IService<HospitalSet> {
}
